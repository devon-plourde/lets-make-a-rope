﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Rope : MonoBehaviour
{
    public Transform RopeLinkPrefab;
    public float throwSpeed;
    Transform Head;
    List<HingeJoint2D> RopeLinks;
    Rigidbody2D headRigid;

    // Start is called before the first frame update
    void Start()
    {
        headRigid = GetComponent<Rigidbody2D>();
        RopeLinks = new List<HingeJoint2D>();
        Head = transform;
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log(transform.eulerAngles);
    }

    public void Throw(Joint2D sourceJoint)
    {
        StartCoroutine(Throwing(sourceJoint));
    }

    IEnumerator Throwing(Joint2D sourceJoint)
    {
        yield return new WaitForSeconds(1);

        Head.parent = null;
        headRigid.bodyType = RigidbodyType2D.Kinematic;
        Head.GetComponent<HingeJoint2D>().enabled = false;

        var startTime = Time.time;
        var radAngle = transform.eulerAngles.z * (Mathf.PI / 180f);
        var linkLength = RopeLinkPrefab.GetComponent<BoxCollider2D>().size.x * RopeLinkPrefab.transform.localScale.x;

        var link = Instantiate(RopeLinkPrefab, transform.position, Quaternion.Euler(transform.eulerAngles), transform);
        link.localScale = new Vector2(0, link.localScale.y);

        link.GetComponent<HingeJoint2D>().enabled = false;
        link.GetComponent<HingeJoint2D>().connectedBody = headRigid;
        RopeLinks.Add(link.GetComponent<HingeJoint2D>());
        sourceJoint.connectedBody = link.GetComponent<Rigidbody2D>();
        sourceJoint.enabled = true;

        float linkDistance = linkLength;
        while(Time.time - startTime < 3)
        {
            var movement = new Vector2(Mathf.Cos(radAngle), Mathf.Sin(radAngle)) * throwSpeed;
            headRigid.MovePosition(new Vector2(movement.x + transform.position.x, movement.y + transform.position.y));
            if (linkDistance >= throwSpeed)
            {
                linkDistance -= throwSpeed;
            }
            else
            {
                linkDistance -= throwSpeed;
                link.localScale = RopeLinkPrefab.localScale;

                linkDistance += linkLength;

                //Create a new link.
                link = Instantiate(RopeLinkPrefab, RopeLinks.Last().transform.position, Quaternion.Euler(transform.eulerAngles), transform);
                link.GetComponent<HingeJoint2D>().enabled = false;

                //Add the link to the list and configure it.
                link.GetComponent<HingeJoint2D>().connectedBody = RopeLinks.Last().GetComponent<Rigidbody2D>();
                sourceJoint.connectedBody = link.GetComponent<Rigidbody2D>();
                RopeLinks.Last().enabled = true;
                RopeLinks.Add(link.GetComponent<HingeJoint2D>());
            }
            link.localScale = new Vector2(
                ((linkLength - linkDistance) / linkLength) * RopeLinkPrefab.localScale.x,
                link.localScale.y);

            yield return new WaitForFixedUpdate();
        }
        RopeLinks.Last().enabled = true;
        for (int i = 0; i < RopeLinks.Count(); ++i)
        {
            RopeLinks[i].GetComponent<Rigidbody2D>().velocity = Vector2.zero;
            RopeLinks[i].GetComponent<Rigidbody2D>().gravityScale = RopeLinkPrefab.GetComponent<Rigidbody2D>().gravityScale;
        }
        headRigid.bodyType = RigidbodyType2D.Dynamic;
        Head.GetComponent<HingeJoint2D>().enabled = true;
    }

    //void AddLink()
    //{
    //    RopeLinks.Last().connectedBody = link.GetComponent<Rigidbody2D>();
    //    link.GetComponent<HingeJoint2D>().connectedBody = headRigid;
    //    RopeLinks.Add(link.GetComponent<HingeJoint2D>());
    //}

    public void StopThrowing()
    {
        headRigid.bodyType = RigidbodyType2D.Dynamic;
        Head.GetComponent<HingeJoint2D>().enabled = true;
    }
}
