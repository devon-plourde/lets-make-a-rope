﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public Camera mainCam;
    public Transform rope;
    public Transform hand;

    public float movement;

    float angle;
    Rope2 ropeInHand;
    Joint2D playerJoint;
    Rigidbody2D rigid;

    // Update is called once per frame
    void Update()
    {
        var mousePosition = mainCam.ScreenToWorldPoint(Input.mousePosition);
        var diff = mousePosition - transform.position;
        angle = Mathf.Atan2(diff.y, diff.x);
        hand.eulerAngles = new Vector3(0, 0, angle / (Mathf.PI / 180f));

        if (Input.GetKeyDown("left shift") && ropeInHand == null)
        {
            var r = Instantiate(rope, hand);
            ropeInHand = r.GetComponent<Rope2>();
        }

        if (Input.GetMouseButtonDown(0) && ropeInHand != null)
        {
            playerJoint = GetComponent<Joint2D>();
            //playerJoint.connectedBody = ropeInHand.GetComponent<Rigidbody2D>();

            //playerJoint.enabled = true;
            ropeInHand.Throw(transform);
        }
        else if (Input.GetMouseButtonUp(0))
        {
            //ropeInHand.StopThrowing();
            ropeInHand = null;
        }

        if (Input.GetKeyDown("space"))
        {
            playerJoint.enabled = false;
            playerJoint.connectedBody = null;
        }

        if (rigid == null)
            rigid = GetComponent<Rigidbody2D>();

        rigid.AddForce(new Vector2(Input.GetAxis("Horizontal") * movement, 0/*rigid.velocity.y*/));
    }
}
