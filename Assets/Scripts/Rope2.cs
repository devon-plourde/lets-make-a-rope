﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rope2 : MonoBehaviour
{
    public Transform RopeLinkPrefab;
    public float throwDistance;
    public float throwTime;

    public LineRenderer line;
    public EdgeCollider2D lineCollider;
    Rigidbody2D headRigid;
    Joint2D headJoint;
    List<Transform> ropeLinks = new List<Transform>();

    Transform player;
    bool grappelled = false;
    bool deployed = false;

    // Start is called before the first frame update
    void Start()
    {
        //line = GetComponent<LineRenderer>();
        //collider = GetComponent<EdgeCollider2D>();
        headRigid = GetComponent<Rigidbody2D>();
        headJoint = GetComponent<Joint2D>();
    }

    public void Throw(Transform p)
    {
        player = p;
        transform.parent = null;
        StartCoroutine(Throwing());
    }

    IEnumerator Throwing()
    {
        float startTime = Time.time;
        var radAngle = transform.eulerAngles.z * (Mathf.PI / 180f);
        while (Time.time - startTime < throwTime && !deployed)
        {
            var movement = new Vector2(Mathf.Cos(radAngle), Mathf.Sin(radAngle)) * throwDistance;
            headRigid.MovePosition(new Vector2(movement.x + transform.position.x, movement.y + transform.position.y));

            var adjustedPlayerPosition = Quaternion.Euler(new Vector3(0, 0, 180)) * (transform.position - player.position);          
            line.SetPositions(new Vector3[] { Vector3.zero, adjustedPlayerPosition });
            lineCollider.points = new Vector2[] { Vector2.zero, adjustedPlayerPosition };
            line.transform.localEulerAngles = new Vector3(0, 0, -transform.eulerAngles.z);
            yield return null;
        }

        Vector2 playerPosition = -lineCollider.points[1];
        radAngle = Mathf.Atan2(playerPosition.y, playerPosition.x);
        float linkLength = RopeLinkPrefab.GetComponent<BoxCollider2D>().size.y * RopeLinkPrefab.localScale.x;
        float numberOfLinks = Mathf.Sqrt(Mathf.Pow(lineCollider.points[1].x, 2) + Mathf.Pow(lineCollider.points[1].y, 2)) / linkLength;
        Transform lastLink = transform;
        for (; numberOfLinks >= 1; --numberOfLinks)
        {
            var newLinkPosition = new Vector2(lastLink.position.x - (Mathf.Cos(radAngle) * linkLength),
                lastLink.position.y - (Mathf.Sin(radAngle) * linkLength));
            var newLink = Instantiate(RopeLinkPrefab, newLinkPosition, Quaternion.Euler(0, 0, radAngle / (Mathf.PI / 180)), transform);
            ropeLinks.Add(newLink);
            var newLinkJoint = newLink.GetComponent<HingeJoint2D>();
            newLinkJoint.connectedBody = lastLink.GetComponent<Rigidbody2D>();
            newLinkJoint.enabled = true;

            lastLink = newLink;
        }
        if(numberOfLinks > 0)
        {
            var newLinkPosition = new Vector2(lastLink.position.x - (Mathf.Cos(radAngle) * linkLength),
                lastLink.position.y - (Mathf.Sin(radAngle) * linkLength));
            var newLink = Instantiate(RopeLinkPrefab, newLinkPosition, Quaternion.Euler(0, 0, radAngle / (Mathf.PI / 180)), transform);
            newLink.localScale = new Vector2(numberOfLinks * RopeLinkPrefab.localScale.x, RopeLinkPrefab.localScale.y);
            ropeLinks.Add(newLink);
            var newLinkJoint = newLink.GetComponent<HingeJoint2D>();
            newLinkJoint.connectedBody = lastLink.GetComponent<Rigidbody2D>();
            newLinkJoint.enabled = true;

            lastLink = newLink;
        }

        line.enabled = false;
        lineCollider.enabled = false;
        var playerJoint = player.GetComponent<Joint2D>();
        playerJoint.connectedBody = lastLink.GetComponent<Rigidbody2D>();
        playerJoint.enabled = true;

        if (grappelled)
        {
            headJoint.enabled = grappelled;
            headRigid.bodyType = RigidbodyType2D.Kinematic;
        }
        else
        {
            headJoint.enabled = grappelled;
            headRigid.bodyType = RigidbodyType2D.Dynamic;
        }
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //Debug.Log("Trigger Enter");
        deployed = true;
        grappelled = false;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        deployed = true;
        grappelled = true;
    }
}
